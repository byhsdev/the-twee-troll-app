Script to use the streaming api of Twitter with OAUTH2 authentication.
Written in Python 2 using Django.

Copyright Antonio S�nchez Pineda

Licensed under GPLv3.

Contact: tech@sanchezantonio.com

This software uses tweepy library.

