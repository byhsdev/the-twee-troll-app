__author__ = "Antonio Sanchez Pineda"
__copyright__ = "Copyright 2014, Antonio Sanchez Pineda"
__credits__ = ["Antonio Sanchez Pineda"]
__license__ = "GPLv3"
__version__ = "1.0.0"
__maintainer__ = "Antonio Sanchez Pineda"
__email__ = "tech@sanchezantonio.com"
__status__ = "Production"

from django.core.management.base import BaseCommand, CommandError
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import tweepy
import simplejson
import re

from chatterbotapi import ChatterBotFactory, ChatterBotType

# Go to http://dev.twitter.com and create an app.
# The consumer key and secret will be generated for you after
consumer_key="YOUR-CONSUMER-KEY"
consumer_secret="YOUR-CONSUMER-SECRET"

# After the step above, you will be redirected to your app's page.
# Create an access token under the the "Your access token" section
access_token="YOUR-OAUTH2-ACCESS-TOKEN"
access_token_secret="YOUR-OAUTH2-ACCESS-TOKEN-SECRET"

auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)
#user = api.me()

#print 'Name: ' + user.name
#print 'ID: ' + str(user.id)

factory = ChatterBotFactory()

bot1 = factory.create(ChatterBotType.CLEVERBOT)
bot1session = bot1.create_session()

class StdOutListener(StreamListener):
    """ A listener handles tweets are the received from the stream.
    This is a basic listener that just prints received tweets to stdout.

    """
    def on_data(self, data):
        try: 
            #print type(data)
            clean_data = simplejson.loads(data)
            #print type(clean_data)
            print clean_data['text']
            text = clean_data['text']
            print clean_data['user']['screen_name']
            who = clean_data['user']['screen_name']

            if who == 'TheTweeTroll':
                return True

            #print clean_data['user']['screen_name']
            print clean_data['id']
            tweetId = clean_data['id']
            
            s = bot1session.think(text);
            insensitive_filter = re.compile(re.escape('cleverbot'), re.IGNORECASE)
            s = insensitive_filter.sub('TheTweeTroll', s)
            s = ''.join(['@', who, ' ', s])

            print s

            api.update_status(s, tweetId)
        except Exception as e:
            print e
        
        return True

    def on_error(self, status):
        print status

class Command(BaseCommand):

    def handle(self, *args, **options):
        l = StdOutListener()

        stream = Stream(auth, l)
        stream.filter(follow=['1626604652'.encode('utf8')], track=['@TheTweeTroll'.encode('utf8')])
